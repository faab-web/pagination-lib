import { PaginationTheadCellSortingComponent } from '../component/pagination-thead-cell/pagination-thead-cell-sorting.component';
import { TranslateModule } from '@ngx-translate/core';
import { PAGINATION_OPTIONS_TOKEN } from '../const/pagination-options-token';
import { AvailableService } from '../service/available.service';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { PaginationModuleOptions } from '../interface/pagination-module-options';
import { PaginationTheadCellResolverComponent } from '../component/pagination-thead-cell/pagination-thead-cell-resolver.component';
import { PaginationTheadCellPlainComponent } from '../component/pagination-thead-cell/pagination-thead-cell-plain.component';
import { PaginationTheadCellDirective } from '../directive/pagination-thead-cell.directive';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [
    PaginationTheadCellSortingComponent,
    PaginationTheadCellPlainComponent,
    PaginationTheadCellResolverComponent,
    PaginationTheadCellDirective
  ],
  imports: [
    CommonModule,
    TranslateModule,
    // LowerCasePipe
  ],
  entryComponents: [
    PaginationTheadCellPlainComponent
  ],
  exports: [
    PaginationTheadCellResolverComponent,
    PaginationTheadCellSortingComponent
  ]
})
export class PaginationLibModule {
  static forRoot(options?: PaginationModuleOptions): ModuleWithProviders {
    return {
      ngModule: PaginationLibModule,
      providers: [
        AvailableService,
        {
          provide: PAGINATION_OPTIONS_TOKEN,
          useValue: options,
        }
      ]
    };
  }
}
