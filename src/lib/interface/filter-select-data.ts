export interface FilterSelectData {
  value: string;
  label: string;
}
