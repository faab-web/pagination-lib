import { PaginationEventData } from './pagination-event-data';
import { PaginationSortingDirectionEnum } from '../enum/pagination-sorting-direction.enum';

export interface PaginationSortingEventData extends PaginationEventData {
  value: PaginationSortingDirectionEnum;
}
