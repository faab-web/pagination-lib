export interface PaginationParamItem {
  name: string;
  value: string;
}
