import { PaginationTheadCellComponentData } from './pagination-thead-cell-component-data';
import { EventEmitter } from '@angular/core';
import { PaginationEvent } from './../model/pagination-event';
import { PaginationProcessData } from './pagination-process-data';
import { PaginationEventData } from './pagination-event-data';
import { PaginationParamItem } from './pagination-param-item';

export interface PaginationTheadCellComponentInterface extends PaginationTheadCellComponentData {
  changePaginationEvent: EventEmitter<PaginationEvent>;
}

export interface PaginationTheadCellStaticInterface {
  EVENT_TYPE: string;
  PARAM_NAME: string[];
  new(): PaginationTheadCellComponentInterface;
  updateEventData(evevnt_data: PaginationEventData, process_data?: PaginationProcessData): number;
  updateEventDataByParamItem(param_item: PaginationParamItem): void;
  getParamItem(): string[];
  generateRequestUrl(): string;
  getValueByKey(key: string): any;
}
