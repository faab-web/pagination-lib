export interface TableColumn {
  key: string;
  label: string;
}

export interface ChangedColumns {
  actual: TableColumn[];
  deleted: TableColumn[];
}
