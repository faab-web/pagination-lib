import {AttributePaginationFilterTypeEnum} from '../enum/attribute-pagination-filter-type.enum';

export interface AttributePaginationFilterDate {
  type: AttributePaginationFilterTypeEnum.DATE;
}
