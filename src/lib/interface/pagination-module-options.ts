import { PaginationSortingDirectionEnum } from '../enum/pagination-sorting-direction.enum';

export interface PaginationModuleOptions {
  sort: string;
  direction: PaginationSortingDirectionEnum;
  max: number;
  filter_components?: any[];
}
