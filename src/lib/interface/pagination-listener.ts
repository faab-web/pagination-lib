import { Updatable } from '@faab/base-lib';
import { PaginationDataSetting } from './pagination-data-setting';

export interface PaginationListener {
  readonly columns_default: string[];
  readonly pager_data_settings: PaginationDataSetting[];
  readonly command_component: string;

  onAttributesInit(): void;

  // is common with MapInterfaces want in MapComponent we must to be available to export data in xlsx format
  onDataUpdate(items: Updatable[]): void;

}
