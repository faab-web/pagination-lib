import { PaginationSortingDirectionType } from '../type/pagination-sorting-direction.type';

export interface PaginationTheadCellComponentData {
  event_type?: string;
  key: string;
  value?: any;
  title: string;
  direction: PaginationSortingDirectionType;
  sortable: boolean;
}
