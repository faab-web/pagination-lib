export interface FilterEvent {
  event: 'from' | 'to' | 'text' | 'direction';
  data: any;
}
