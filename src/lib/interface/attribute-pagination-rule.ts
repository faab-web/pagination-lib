import { AttributePaginationFormatEnum } from './../enum/attribute-pagination-format.enum';

export interface AttributePaginationRule {
  format: AttributePaginationFormatEnum;
  readonly?: boolean;
  xlsx_width?: number;
  label?: string;
  sortable?: boolean;
  field?: string;
  localized?: boolean;
  required?: boolean;
  placeholder?: string;
  dynamic?: boolean;
  fileId?: string;
  unique?: boolean;
  class?: {[key: string]: boolean} | string;
  codeTemplate?: string;
  regular?: RegExp;
  plain?: boolean;
  data?: any;
  filter?: {
    type: string,
    data?: any
  }
}
