export interface PaginationController {

  getTitle(): string;

  getExpectedClass(): any;

  getColumns(): any;

  getDataUrl(): string;

  getBaseUrl(): string;

  onItemClick(ob: any): void;

  getControls(): any;

  getSort(): string;

  isClickable(): boolean;
}
