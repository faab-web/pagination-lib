import {PaginationSettingTypeEnum} from '../enum/pagination-setting-type.enum';

export interface PaginationDataSetting {
  title?: string;
  hide?: boolean;
  only?: string;
  ic_class?: string;
  type: PaginationSettingTypeEnum;
}
