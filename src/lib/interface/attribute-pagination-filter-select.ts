import {AttributePaginationFilterTypeEnum} from '../enum/attribute-pagination-filter-type.enum';
import {FilterSelectData} from './filter-select-data';

export interface AttributePaginationFilterSelect {
  type: AttributePaginationFilterTypeEnum.SELECT;
  field: string;
  data: FilterSelectData[];
}
