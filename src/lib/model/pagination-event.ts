import { PaginationEventData } from '../interface/pagination-event-data';

export class PaginationEvent {
  type: string;
  data: PaginationEventData;
  constructor(type: string, data: PaginationEventData) {
    this.type = type;
    this.data = data;
  }
}
