import { AttributePaginationRule } from './../interface/attribute-pagination-rule';
import { AttributePaginationFormatEnum } from '../enum/attribute-pagination-format.enum';
import { Updatable } from '@faab/base-lib';

export class PaginationModel extends Updatable {
  getRules(): {[key: string]: AttributePaginationRule} {
    return {
      uuid: {
        format: AttributePaginationFormatEnum.VIEW
      },
      whenCreated: {
        format: AttributePaginationFormatEnum.DATE,
        readonly: true,
        xlsx_width: 10
      },
      whenUpdated: {
        format: AttributePaginationFormatEnum.DATE,
        readonly: true,
        xlsx_width: 10
      }
    };
  }

}
