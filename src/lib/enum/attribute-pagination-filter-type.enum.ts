export enum AttributePaginationFilterTypeEnum {
  SELECT = 'select',
  TEXT = 'text',
  DATE = 'date',
  PLAIN = 'plain'
}
