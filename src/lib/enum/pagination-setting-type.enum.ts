export enum PaginationSettingTypeEnum {
  SWITCH_MODE = 'switch_mode',
  AVAILABLE = 'available',
  COLUMNS = 'columns',
  XLSX = 'xlsx',
  REFRESH = 'refresh',
  SWITCH_BASKET_TYPE = 'switch_basket_type',
}
