export enum PaginationSortingDirectionEnum {
  DESC = 'desc',
  ASC = 'asc'
}
