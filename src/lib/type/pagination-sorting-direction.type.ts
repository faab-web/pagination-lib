import { PaginationSortingDirectionEnum } from '../enum/pagination-sorting-direction.enum';

export type PaginationSortingDirectionType = PaginationSortingDirectionEnum | null;
