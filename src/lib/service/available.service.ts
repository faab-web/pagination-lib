import {EventEmitter, Injectable, Output} from '@angular/core';

@Injectable()
export class AvailableService {

  private _available: boolean = true;

  @Output() change: EventEmitter<boolean> = new EventEmitter();

  constructor() {

  }

  get available(): boolean {
    return this._available;
  }

  set available(value: boolean) {
    if (this._available === value) {
      return;
    }
    this._available = value;
    this.change.emit(value);
  }


  toggle(): boolean {
    this.available = !this.available;
    return this.available;
  }

}
