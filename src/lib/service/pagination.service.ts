import { PAGINATION_OPTIONS_TOKEN } from './../const/pagination-options-token';
import { PaginationModuleOptions } from './../interface/pagination-module-options';
import { Injectable, Inject, EventEmitter, Output } from '@angular/core';
import { PaginationSortingDirectionEnum } from '../enum/pagination-sorting-direction.enum';

@Injectable({
  providedIn: 'root'
})
export class PaginationService {

  @Output() change: EventEmitter<string> = new EventEmitter<string>();

  public config: EventEmitter<PaginationModuleOptions> = new EventEmitter();
  private filter_components: any[];

  constructor(
    @Inject(PAGINATION_OPTIONS_TOKEN) private token_config: PaginationModuleOptions,
  ) {
    const config_default: PaginationModuleOptions = {
      sort: 'whenCreated',
      direction: PaginationSortingDirectionEnum.DESC,
      max: 20,
      filter_components: []
    };
    if (token_config) {
      if (typeof token_config.sort === 'string') {
        config_default.sort = token_config.sort;
      }
      if (typeof token_config.direction === 'string' && PaginationSortingDirectionEnum[token_config.direction.toUpperCase()]) {
        config_default.direction = token_config.direction as PaginationSortingDirectionEnum;
      }
      if (typeof token_config.max === 'number') {
        config_default.max = token_config.max;
      }
      if (Array.isArray(token_config.filter_components)) {
        config_default.filter_components = token_config.filter_components;
      }
    }
    this.filter_components = config_default.filter_components;
    this.config.emit(config_default);
  }

  getComponentByEventType(event_type: string, default_component: any): any {
    if (
      typeof event_type !== 'string'
      || !Array.isArray(this.filter_components)
    ) {
      return default_component;
    }
    const filter_components_length: number = this.filter_components.length;
    for (let i = 0; i < filter_components_length; i++) {
      const filter_component: any = this.filter_components[i];
      if (
        !filter_component
        || typeof filter_component.EVENT_TYPE !== 'string'
        || filter_component.EVENT_TYPE !== event_type
      ) {
        continue;
      }
      return filter_component;
    }
    return default_component;
  }

  getAllComponents(): any[] {
    /* console.log('-------------------getAllComponents----------------------');
    console.log(this.filter_components); */
    return (Array.isArray(this.filter_components)) ? this.filter_components : [];
  }
}
