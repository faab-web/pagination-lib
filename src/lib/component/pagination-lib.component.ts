import { PaginationTheadCellPlainComponent } from './pagination-thead-cell/pagination-thead-cell-plain.component';
import { PaginationTheadCellSortingComponent } from './pagination-thead-cell/pagination-thead-cell-sorting.component';
import { PaginationEvent } from './../model/pagination-event';
import { AttributePaginationFormatEnum } from './../enum/attribute-pagination-format.enum';
import { PaginationModel } from './../model/pagination-model';
// import { AttributePaginationFilterTypeEnum } from './../enum/attribute-pagination-filter-type.enum';
import { AttributePaginationRule } from './../interface/attribute-pagination-rule';
import { LanguageLibService } from '@faab/language-lib';
import { AvailableService } from './../service/available.service';
import { ChangedColumns, TableColumn } from './../interface/column';
import { PaginationController } from './../interface/pagination-controller';
import { StorageService } from '@faab/storage-lib';
import { HttpService, LoadingEnum, ResponseInterfaces, HeaderEnum } from '@faab/http-lib';
import { Subscription, Observable } from 'rxjs';
import { PaginationService } from './../service/pagination.service';
import { OnInit, OnDestroy, Input, EventEmitter, Output } from '@angular/core';
import { PaginationListener } from '../interface/pagination-listener';
import { PaginationModuleOptions } from '../interface/pagination-module-options';
import { PaginationSortingDirectionEnum } from '../enum/pagination-sorting-direction.enum';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { DatePipe, Location } from '@angular/common';
import { PaginationTheadCellComponentData } from '../interface/pagination-thead-cell-component-data';
import { PaginationSortingDirectionType } from '../type/pagination-sorting-direction.type';
import { isString } from 'util';

export class PaginationLibComponent implements OnInit, OnDestroy {

  static PARAM_FIELD: string = 'param';
  static PROPERTY_PAGE: string = 'page';


  static SEPARATOR_VALUE: string = '_';
  static SEPARATOR_PROPERTY: string = ';';
  static SEPARATOR_MANY_VALUE: string = ',';
  static SEPARATOR_FILTER_COLLECTION_ITEM: string = '=';

  static COMPONENT_NAME: string = 'Pagination';

  // public _items_export: PaginationModel[];
  // public export_xlsx_exit: boolean;
  // public count_items_export: number;
  // public modalRefProgressExportXlsx: BsModalRef;

  public _items: PaginationModel[];
  public items_length: number = 0;
  public attributes: {[key: string]: AttributePaginationRule};
  public listener: PaginationListener;

  public countPage: number;
  public countPage_prev: number;
  public countPageWithoutFilters: number;
  public page: number = 1;
  public direction: PaginationSortingDirectionEnum = PaginationSortingDirectionEnum.DESC;
  public direction_default: PaginationSortingDirectionEnum = PaginationSortingDirectionEnum.DESC;
  public urlParams = [];
  public visiblePagination = false;
  public from: number;
  public to: number;
  // public filter: string;
  public labels: {[key: string]: string};
  public columns: Observable<any>;
  public cacheKey: string;
  public dateFieldParam = {
    from: null,
    to: null
  };

  private _sort: string = 'whenCreated';
  private _sort_default: string = 'whenCreated';
  private max: number = 20;
  // readonly max_export: number = 100;

  protected generic: any;
  protected _available: boolean;
  protected prev: { server: string, url: string } = {server: '', url: ''};
  protected process_items_after_request: number;
  protected table_title: string;
  protected _subscription_available: Subscription;
  protected _subscription_activatedRoute_queryParams: Subscription;
  protected _subscription_excel_export_start: Subscription;
  protected _subscription_excel_export_ready: Subscription;

  get available(): boolean {
    return this._available;
  }

  set available(value: boolean) {
    this._available = value;
  }

  @Input() clazz: any;
  @Input() url: string;
  @Input() server: string;
  @Input() keys: string[];
  @Input() initFilter: any;

  @Input() set sort(_sort: string) {
    if (typeof _sort !== 'string') {
      return;
    }
    this._sort = _sort;
  }

  @Input() set initialization(controller: PaginationController) {
    this.setInitialization(controller);
  }

  @Output() changeColumnsActual: EventEmitter<ChangedColumns> = new EventEmitter<ChangedColumns>();

  // @ViewChild('modal_progress_export_xlsx') modal_progress_export_xlsx;

  private subscription_config: Subscription;
  private component_default: any = PaginationTheadCellPlainComponent;

  private config: PaginationModuleOptions;

  constructor(
    protected http: HttpService,
    protected storage: StorageService,
    protected location: Location,
    protected router: Router,
    protected availableService: AvailableService,
    protected activatedRoute: ActivatedRoute,
    protected language: LanguageLibService,
    // protected xlsxService: XlsxService,
    // protected tr: TranslateService,
    protected datePipe: DatePipe,
    // protected modalService: ModalService,
    protected paginationService: PaginationService
  ) {
// tslint:disable-next-line: no-unused-expression
    this.paginationService.config;
    this.subscription_config = this.paginationService.config.subscribe((config_new: PaginationModuleOptions) => {
      if (!config_new) {
        return;
      }
      this.config = config_new;
      this.setConfig();
    });
    this.available = this.storage.getAvailable();
    this._items = [];
  }

  ngOnInit(): void {
    this.onInit();
  }

  onInit(): void {
    this.items_length = 0;
    this.countPage = 0;
    this.countPageWithoutFilters = 0;
  }

  ngOnDestroy(): void {
    this.onDestroy();
  }

  onDestroy(): void {
    this.checkSubscription(this._subscription_available);
    this.checkSubscription(this._subscription_activatedRoute_queryParams);
    this.checkSubscription(this._subscription_excel_export_start);
    this.checkSubscription(this._subscription_excel_export_ready);
  }

  /* onClickStopExportXlsx(e: Event) {
    if(!(e instanceof Event)) {
      return;
    }
    e.preventDefault();
    e.stopPropagation();
    this.export_xlsx_exit = true;
  } */

  onChangedColumnsActual(changed_columns: ChangedColumns): void {
    /* if (!changed_columns || !Array.isArray(changed_columns.actual) || !Array.isArray(changed_columns.deleted)) {
      return;
    }
    this.changeColumnsActual.emit(changed_columns);

    const length_deleted: number = changed_columns.deleted.length,
      param_indexes: number[] = [];

    let filter_str = (this.filter) ? this.filter : '';

    const filters: string[] = (filter_str) ? filter_str.split(PaginationLibComponent.SEPARATOR_MANY_VALUE) : [];

    let length_filters: number = filters.length;
    // direction: string = undefined;

    for (let i = 0; i < length_deleted; i++) {
      const column: TableColumn = changed_columns.deleted[i];
      if (!column || !column.key || typeof column.key !== 'string') {
        continue;
      }
      const filter_name: string = this.getFilterName(column.key);
      if (filter_str.includes(filter_name)) {
        for (let j = 0; j < length_filters; j++) {
          const filter: string = filters[j];
          if (!filter || typeof filter !== 'string' || filter.substring(0, filter_name.length) !== filter_name) {
            continue;
          }
          filters.splice(j, 1);
          length_filters = filters.length;
          filter_str = filters.join(PaginationLibComponent.SEPARATOR_MANY_VALUE);
          break;
        }
      }

    }

    const keys: string[] = [],
      length_actual: number = changed_columns.actual.length;
    for (let i = 0; i < length_actual; i++) {
      const column: TableColumn = changed_columns.actual[i];
      if (!column || !column.key || typeof column.key !== 'string') {
        continue;
      }
      keys.push(column.key);
    }
    this.keys = keys;
    this.filter = filter_str;
    const params_new: string = this.genParams();

    if (this.urlParams[PaginationLibComponent.PARAM_FIELD] === params_new) {
      this.onPagerInit();
      return;
    }

    this.urlParams[PaginationLibComponent.PARAM_FIELD] = params_new;
    this.write();
    this.attributesInit();

    const params_object: Params = {};
    params_object[PaginationLibComponent.PARAM_FIELD] = params_new;

    this.router.navigate(['.'], {
      queryParams: params_object,
      relativeTo: this.activatedRoute
    }); */
  }

  public isSortableField(key: string): boolean {
    return (this.attributes && this.attributes[key] && typeof this.attributes[key].sortable === 'boolean')
    ? this.attributes[key].sortable : false;
  }

  getAttributeTitleByKey(key: string): string {
    return (
      typeof key !== 'string'
      ||
      !this.attributes
      || !this.attributes[key]
      || typeof this.attributes[key].label !== 'string'
    ) ? '' : this.attributes[key].label;
  }

  public getDirection(key: string): PaginationSortingDirectionType {
    return this._sort === key ? this.direction : null;
  }

  /* public getFilterName(key: string) {
    return (this.attributes[key].filter && this.attributes[key].filter['field'])
    ? this.attributes[key].filter['field'] : key;
  } */

  getPaginationTheadCellComponentData(key: string): PaginationTheadCellComponentData {
    // console.log('getPaginationTheadCellComponentData => (k: ' + key + ')');
    if (typeof key !== 'string') {
      return undefined;
    }
    const event_type: string = this.getColumnType(key),
      component: any = this.paginationService.getComponentByEventType(event_type, this.component_default);
    if (!this.isValidComponent(component)) {
      return undefined;
    }
    const component_data: PaginationTheadCellComponentData = {
        key: key,
        title: this.getAttributeTitleByKey(key),
        direction: this.getDirection(key),
        sortable: this.isSortableField(key),
        event_type: this.getColumnType(key)
      };
    if (event_type !== this.component_default['EVENT_TYPE']) {
      component_data.value = component['getValueByKey'](key);
    } else {
      // console.log('NO VALUE');
    }
    /* console.log('--------------------------');
    console.log(component_data); */
    return component_data;
  }

  /* public getSelected(key: string): string {
    key = this.getFilterName(key);
    if (!this.filter) {
      return '';
    }

    let result = '';
    try {
      const filter = this.filter.split(PaginationLibComponent.SEPARATOR_MANY_VALUE);
      filter.forEach(val => {
        if (val.search(key) === 0) {
          result = val.split('=')[1];
          throw Error();
        }
      });
    } catch (e) {
      // skip error
    }
    return result;
  } */

  /* public getExportProgress(): number {
    const current_items: number = this._items_export.length,
      request_part: number = 98,
      after_request_part: number = 100 - request_part;
    if(!current_items || !this.count_items_export) {
      return 0;
    }
    if(this.count_items_export > current_items) {
      return Math.round((this._items_export.length / this.count_items_export) * request_part);
    }
    return request_part + Math.round((this.process_items_after_request / this.count_items_export) * after_request_part );
  } */

  protected setInitialization(controller: any): void {
    if (!controller) {
      return;
    }
    this.url = controller.getDataUrl();
    this.clazz = controller.getExpectedClass();
    this.keys = controller.getColumns();
    this.server = controller.getBaseUrl();
    this._sort = controller.getSort();
    this.table_title = controller.getTitle();
  }

  protected preInit() {
    this.generic = new this.clazz;
    this.attributes = this.generic.getRules();
    this.labels = this.generic.getLabels();

    if (!this.keys) {
      this.keys = Object.keys(this.attributes);
    }
    // tslint:disable-next-line: deprecation
    this.columns = Observable.create((o: any) => {
      o.next(this.attributes);
    });
  }

  protected onPagerInit() {
    this.initCacheKey();
    if (!this.attributes) {
      this.preInit();
    }
    this.attributesInit();
    this.checkSubscription(this._subscription_activatedRoute_queryParams);
    this._subscription_activatedRoute_queryParams = this.activatedRoute.queryParams.subscribe(params => {
      this.processParams(params);
    });
    this.checkSubscription(this._subscription_available);
    this._subscription_available = this.availableService.change.subscribe( (available: boolean) => {
      this._available = available;
      this.pageChange(1);
    });
    /* this.checkSubscription(this._subscription_excel_export_start);
    this._subscription_excel_export_start = this.xlsxService.onExportStart.subscribe((is_started: boolean) => {
      if (is_started !== true) {
        return;
      }
      this.modalRefProgressExportXlsx = this.modalService.openModal(this.getModals(), 'ModalProgressExportXlsx');

      this.xlsxService.onExportStart.next(false);
      this.export_xlsx_exit = false;
      this.count_items_export = 0;
      this.process_items_after_request = 0;
      this._items_export = [];

      setTimeout(() => {
        this.getData(0);
      }, 500);

    }) */

    /* this.checkSubscription(this._subscription_excel_export_ready);
    this._subscription_excel_export_ready = this.xlsxService.onExportReady.subscribe((is_ready: boolean) => {
      if(is_ready !== true) {
        return;
      }
      this.xlsxService.onExportReady.next(false);

      this.getData(0);
    }) */
  }

  public initCacheKey() {
    const initFilter: string = (this.initFilter) ? this.initFilter.toString() : '';
    this.cacheKey = PaginationLibComponent.COMPONENT_NAME + initFilter + this.getUrl();
  }

  protected getUrl(): string {
    return '';
  }

  /* protected getModals(): ModalBs[] {
    return [
      {
        content: this.modal_progress_export_xlsx,
        id: 'ModalProgressExportXlsx',
        config: {class: 'modal-md', ignoreBackdropClick: true}
      }
    ];
  } */

  protected processParams(params: Params) {
    this.urlParams = Object.assign([], params);
    const _params: string = params[PaginationLibComponent.PARAM_FIELD];
    if (!_params) {
      const cache = this.storage.readCache(this.cacheKey);
      this.readAndInitParam((cache) ? cache : 'available');
      this.pageChange(this.page);
      return;
    }
    this.readAndInitParam(_params);
    this.getData();
  }

  protected readAndInitParam(params: string) {

    if (this.initFilter) {
      params = params + PaginationLibComponent.SEPARATOR_PROPERTY + this.initFilter;
    }
    if (!params) {
      return;
    }
    const _params: string[] = params.split(PaginationLibComponent.SEPARATOR_PROPERTY),
      components: any[] = this.paginationService.getAllComponents(),
      components_length: number = components.length;
    // debugger;
    _params.forEach(
      (param: string) => {
        const p: string[] = param.split(PaginationLibComponent.SEPARATOR_VALUE),
          p_length: number = p.length;
        switch (p[0]) {
          case PaginationLibComponent.PROPERTY_PAGE:
            this.page = parseInt(p[1], 10);
            this.countPage = parseInt(this.storage.readCache(this.cacheKey + 'count'), 10);
            break;
          case PaginationTheadCellSortingComponent.PARAM_NAME_DIRECTION:
            this.direction = (
              typeof p[1] === 'string'
              && PaginationSortingDirectionEnum[p[1].toLocaleUpperCase()]
            ) ? p[1] as PaginationSortingDirectionEnum : this.direction_default;
            break;
          case PaginationTheadCellSortingComponent.PARAM_NAME_SORT:
            this._sort = p[1];
            break;
          /* case 'from':
            this.from = parseInt(p[1], 10);
            break;
          case 'to':
            this.to = parseInt(p[1], 10);
            break; */
          /* case 'filter':
            try {
              const self = this;
              const filter_param: string = (p_length > 2) ?
                p.slice(1, p_length).join(PaginationLibComponent.SEPARATOR_VALUE) : p[1];
              filter_param.split(PaginationLibComponent.SEPARATOR_MANY_VALUE).forEach(value => {
                self.genFilterField(value);
              });
            } catch (e) {
              // skip error
            }
            break; */
          /*case 'not-available':
             if (this.available) {
              if (!(this.generic instanceof Basket) && !(this.generic instanceof User)) {
                this.page = 1;
              }
            }
            this.availableService.available = this.available;
            break;
            */
          default:
            for (let i = 0; i < components_length; i++) {
              const component: any = components[i];
              /* console.log('component №' + i);
              console.log(component); */
              if (isString(component.EVENT_TYPE)) {
                console.log(component.EVENT_TYPE);
              }
              if (!this.isValidComponent(component)) {
                continue;
              }
              console.log(p);
              // debugger;
              component['updateEventDataByParamItem']({
                name: p[0],
                value: p[1]
              });
            }
          }
      }
    );

    const convert = new Date();
    let _f = null;
    let _t = null;
    if (this.from) {
      convert.setTime(this.from);
      _f = {year: convert.getFullYear(), month: convert.getMonth() + 1, day: convert.getDate()};
    }
    if (this.to) {
      convert.setTime(this.to - 3600 * 24 * 1000);
      _t = {year: convert.getFullYear(), month: convert.getMonth() + 1, day: convert.getDate()};
    }
    this.dateFieldParam = {
      from: _f,
      to: _t
    };
    this.changeUrl();
  }

  protected genParams() {
    const params = [];
    if (this.page > 1) {
      params.push(PaginationLibComponent.PROPERTY_PAGE + PaginationLibComponent.SEPARATOR_VALUE + this.page);
    }
    if (this.direction !== this.direction_default) {
      params.push(PaginationTheadCellSortingComponent.PARAM_NAME_DIRECTION + PaginationLibComponent.SEPARATOR_VALUE + this.direction);
    }
    if (this._sort !== this._sort_default) {
      params.push(PaginationTheadCellSortingComponent.PARAM_NAME_SORT + PaginationLibComponent.SEPARATOR_VALUE + this._sort);
    }
    if (this.from) {
      params.push('from' + PaginationLibComponent.SEPARATOR_VALUE + this.from);
    }
    if (this.to) {
      params.push('to' + PaginationLibComponent.SEPARATOR_VALUE + this.to);
    }
    const components: any[] = this.paginationService.getAllComponents(),
      components_length: number = components.length;
    for (let i = 0; i < components_length; i++) {
      const component: any = components[i];
      if (!this.isValidComponent(component)) {
        continue;
      }
      const param_items: string[] = component['getParamItem'](),
      param_items_length: number = param_items.length;
      for (let j = 0; j < param_items_length; j++) {
        const param_item: string = param_items[j];
        if (param_item) {
          params.push(param_item);
        }
      }
    }
    /* if (this.filter) {
      params.push('filter' + PaginationLibComponent.SEPARATOR_VALUE + this.filter);
    } */
    if (!this.available) {
      params.push('not-available');
    } else {
      params.push('available');
    }
    return params.join(PaginationLibComponent.SEPARATOR_PROPERTY);
  }

  /* protected genFilterField(newValue: string) {
    const arr = newValue.split('=');
    const newFilter = [];
    let isNew = true;

    if (this.filter) {
      const filters = this.filter.split(PaginationLibComponent.SEPARATOR_MANY_VALUE);
      filters.forEach(value => {
        if (value.search(arr[0]) === 0) {
          isNew = false;
          if (!arr[1]) {
            return;
          }
          value = newValue;
        }
        newFilter.push(value);
      });
    }
    if (isNew) {
      newFilter.push(newValue);
    }
    this.filter = newFilter.length > 0 ? newFilter.join(PaginationLibComponent.SEPARATOR_MANY_VALUE) : undefined;
  } */

  protected generatorRequestURL(/* export_first?: number */) {
    const first: number = (this.page - 1) * this.max/* isNaN(export_first) ? (this.page - 1) * this.max : export_first */,
      max: number = this.max /* isNaN(export_first) ? this.max : this.max_export */,
      available: string = this.available ? 'available&' : '';
    let _url = this.url + '?' + available + 'first=' + first + '&max=' + max + '&sort=' + this._sort + '&direction=' + this.direction;
    if (this.from) {
      _url += '&from=' + this.from;
    }
    if (this.to) {
      _url += '&to=' + this.to;
    }
    /* if (this.filter) {
      const arr = this.filter.split(PaginationLibComponent.SEPARATOR_MANY_VALUE);
      arr.forEach(value => {
        _url += '&filter_' + value;
      });
    } */
    const components: any[] = this.paginationService.getAllComponents(),
      components_length: number = components.length;
    for (let i = 0; i < components_length; i++) {
      const component: any = components[i];
      if (!this.isValidComponent(component)) {
        continue;
      }
      _url += component['generateRequestUrl']();
    }
    return _url;
  }

  protected write() {
    this.storage.writeСache(this.cacheKey, this.urlParams[PaginationLibComponent.PARAM_FIELD]);
    this.storage.setAvailable(this.available);
  }

  public get(key: string, item: PaginationModel): string {
    let localized = false;
    let dynamic = false;
    try {
      try {
        localized = this.attributes[key].format === AttributePaginationFormatEnum.LOCALIZED
        || this.attributes[key].format === AttributePaginationFormatEnum.LOCALIZED_TEXTAREA;
        dynamic = this.attributes[key].dynamic;
      } catch (e) {
        // ignore
      }

      const arr_key: string[] = key.split('.');
      if (key.length > 1) {
        if (localized) {
          return this.getLocaleName(arr_key, item);
        }
        const obj: string | object = this.executeFunctionByName(arr_key, item);
        if (typeof obj === 'string') {
          return obj;
        }
        if (typeof obj === 'object') {
          return 'ERROR TYPE';
        }
      } else {
        if (localized) {
          return this.getLocaleName(arr_key, item);
        }
        return item[key].length === 0 ? 'dashboard.empty' : item[key] + '';
      }
    } catch (e) {
      if (dynamic) {
        return '<img width="14px" height="14px" src="/assets/images/spinner_loading_small_v2.gif" />';
      } else {
        return 'dashboard.empty';
      }
    }
  }

  public getNumber(key: string, item: PaginationModel) {
    const num = this.get(key, item);
    return num + '';
  }

  public getBoolean(key: string, item: PaginationModel) {
    let a = this.get(key, item);
    if (a === 'true') {
      a = 'boolean.true';
    } else if (a === 'false') {
      a = 'boolean.false';
    }
    return a;
  }

  public getCurrency(key: string, item: PaginationModel) {
    const value: string | object = this.get(key, item);
    const a = parseInt(typeof value === 'string' ? value : '', 2);
    if (isNaN(a)) {
      return a;
    }
    return (a / 100) + '';
  }

  public getAccordion(key: string, item: PaginationModel) {
    let value: string[];
    let field: string;
    if (key.indexOf('.') >= 0) {
      value = key.split('.');
      field = value[value.length - 1];
      value.pop();
      key = value.join('.');
    }
    return item[key];
  }

  public getColumnType(key: string): string {
    return (
      this.attributes
      && this.attributes[key]
      && this.attributes[key].filter
      && typeof this.attributes[key].filter.type === 'string'
    ) ? this.attributes[key].filter.type : this.component_default['EVENT_TYPE'];
  }

  protected getDate(key: string, item: PaginationModel): number {
    const date: string | object = this.get(key, item);
    return parseInt(typeof date === 'string' ? date : '', 2) + this.getLangOffset();
  }

  public setData(item: PaginationModel[]) {

  }

  public refresh() {
    this.prev = {server: '', url: ''};
    this.getData();
  }

  public clearCache() {
    this.storage.writeСache(this.cacheKey, null);
    this.storage.writeСache(this.cacheKey + 'count', null);
    this.prev = {server: '', url: ''};
  }

  protected getBaseUrl() {
    return this.location.path().split('?')[0];
  }

  protected getLabels(key: string): string {
    return (typeof key === 'string' && this.labels[key]) ? this.labels[key] : '';
  }

  protected getLocaleName(key: string[], item: object): string {
    const el: string | object = this.executeFunctionByName(key, item);
    if (typeof el === 'string') {
      return el;
    }
    if (typeof el === 'object') {
      for (const elKey in el) {
        if (el[elKey].language === this.language.getCurrentLanguage()) {
          return el[elKey].value;
        }
      }
    }
    return '';
  }

  // TODO: костыль чтобы обновлялась дата при смене локализации
  protected getLangOffset() {
    const curLang = this.language.getCurrentLanguage();
    return curLang === 'ru' ? 2 : 1;
  }

  onFilterChange(event: PaginationEvent): void {
    debugger;
    let page = 1;
    switch (event.type) {
      /* case 'from':
        try {
          const date = new Date(event.data.year, event.data.month - 1, event.data.day);
          this.from = date.getTime();
        } catch (e) {
          return;
        }
        break;
      case 'to':
        try {
          const date = new Date(event.data.year, event.data.month - 1, event.data.day);
          this.to = date.getTime() + 3600 * 24 * 1000;
        } catch (e) {
          return;
        }
        break;
      case 'text':
        try {
          this.genFilterField(event.data);
        } catch (e) {
          return;
        }
        break; */
      case PaginationTheadCellSortingComponent.EVENT_TYPE:
        if (event.data && isString(event.data.key) && isString(event.data['value'])) {
          this._sort = event.data.key;
          this.direction = event.data['value'];
          page = this.page;
        }
        break;
      default:
        const component: any = this.paginationService.getComponentByEventType(event.type, this.component_default);
        if (this.isValidComponent(component)) {
          page = component['updateEventData'](event.data);
        }

    }
    this.pageChange(page);
  }

  protected changeUrl() {
    const paramNew = this.genParams();
    if (this.urlParams) {
      this.urlParams[PaginationLibComponent.PARAM_FIELD] = paramNew;
    }
    if (!paramNew) {
      delete this.urlParams[PaginationLibComponent.PARAM_FIELD];
    }
    this.write();
    return this.urlParams;
  }

  public pageChange(page) {
    this.countPage_prev = page === 1 ? 1 : this.countPage;
    this.page = page;
    this.router.navigate([this.getBaseUrl()], {queryParams: this.changeUrl()});
  }

  protected getData(/* export_first?: number */) {
    /* if(this.export_xlsx_exit && !isNaN(export_first)) {
      return;
    } */

    const serve = this.server;
    const url = this.generatorRequestURL(/* export_first */);

    if (this.prev && this.prev.url === url && this.prev.server === this.server) {
      return;
    }
    this.prev = {server: this.server, url: url};


    /* if(isNaN(export_first)) {
      // debugger;
      this._items_export = undefined;
      if(this.prev && this.prev.url === url && this.prev.server === this.server) {
        return;
      }
      this.prev = {server: this.server, url: url};
    } else {
      // debugger;
      if(!Array.isArray(this._items_export)) {
        this._items_export = [];
      }
    } */
    this.runRequest(url/* , export_first */);
  }

  protected runRequest(url: string/* , export_first?: number */) {
    /* if(this.export_xlsx_exit && !isNaN(export_first)) {
      return;
    } */
    if ( this.countPage_prev /* && isNaN(export_first) */) {
      // debugger;
      this.countPage = this.countPage_prev;
    }
    const self = this;
    this.http
      .server(this.server)
      .loading(LoadingEnum.FULL/* isNaN(export_first) ? LoadingEnum.FULL : LoadingEnum.DISABLED */)
      .cache(false)
      .get<PaginationModel[]>(url,
        <ResponseInterfaces<PaginationModel[]>>{
          success(items: PaginationModel[], response) {

            // debugger;

            /* if (this.export_xlsx_exit && !isNaN(export_first)) {
              return;
            } */

            let count_items: number = parseInt(response.headers.get(HeaderEnum.X_TOTAL_COUNT), 2);
            if (isNaN(count_items)) {
              count_items = 0;
            }

            this.items_length = items.length;

            // debugger;

            /* if(!isNaN(export_first)) {
              self.count_items_export = count_items;
              export_first = export_first + self.max_export;
              self._items_export = self._items_export.concat(items);
              if(export_first > count_items) {
                self.xlsxPrepare();
              } else {
                self.getData(export_first);
              }
              return;
            } */

            self.setData(items);

            self.countPage = count_items;
            self.visiblePagination = self.countPage > self.max;
            if (self.countPage !== count_items) {
              self.storage.writeСache(self.cacheKey + 'count', self.countPage);
            }

          },
          failed(err) {

          }
        });

    const url_arr: string[] = url.split('?');
    if (typeof url_arr[1] === 'string') {
      const args: string[] = url_arr[1].split('&'),
        args_length: number = args.length,
        args_full: string[] = [];
      for (let i = 0; i < args_length; i++) {
        const arg: string = args[i];
        if (this.initFilter && this.initFilter === arg) {
          args_full.push(arg);
          continue;
        }
        // @TODO: develop static method in filter component 'skipParamByTotal'
        // remove request (filter) param to find total items without filters
        if (arg.substring(0, 7) === 'filter_' || arg.substring(0, 5) === 'from=' || arg.substring(0, 3) === 'to=') {
          continue;
        }
        if (arg.substring(0, 4) === 'max=') {
          args_full.push('max=1');
          continue;
        }
        args_full.push(arg);
      }
      url_arr[1] = args_full.join('&');
    }
    this.http
      .server(this.server)
      .loading(LoadingEnum.FULL/* isNaN(export_first) ? LoadingEnum.FULL : LoadingEnum.DISABLED */)
      .cache(false)
      .get<PaginationModel[]>(url_arr.join('?'),
        <ResponseInterfaces<PaginationModel[]>>{
          success(items: PaginationModel[], response) {
            /* if(this.export_xlsx_exit && !isNaN(export_first)) {
              return;
            } */
            const count_items: number = parseInt(response.headers.get(HeaderEnum.X_TOTAL_COUNT), 2);
            self.countPageWithoutFilters = isNaN(count_items) ? 0 : count_items;
          },
          failed(err) {
            debugger;
          }
        });
  }

  /* protected xlsxPrepare() {
    if(this.export_xlsx_exit) {
      return;
    }
    if (this.listener) {
      this.listener.onDataUpdate(this._items_export);
    }
  } */

  /* public xlsxExport() {

    if(this.export_xlsx_exit || typeof this._items_export === 'undefined') {
      return;
    }

    const data: PaginationModel[] = this._items_export;

    const items_length: number = this._items_export.length,
      keys_length: number = this.keys.length,
      items: any[] = [],
      empty: string = 'dashboard.empty';

    const xlsx_width_cols: ColInfo[] = [];

    for(let i = 0; i < items_length; i++) {
      if(this.export_xlsx_exit) {
        return;
      }
      const item: PaginationModel = data[i],
        row = {};
      for(let j = 0; j < keys_length; j++) {
        const key = this.keys[j];
        if(!this.attributes || !key || !this.attributes[key] || !this.attributes[key].format || !this.attributes[key].label ) {
          continue;
        }
        const attribute = this.attributes[key],
          label: string = attribute.label,
          xlsx_width_col: ColInfo = {wch: (!isNaN(attribute.xlsx_width)) ? attribute.xlsx_width : 20},
          label_i18n: string = this.tr.instant(label);
        let format: string = attribute.format;
        if(format.search('date:') === 0) {
          let date_format: string = 'dd-MM-yyyy';
          if(format.substring(0, 6) === 'date::' && format !== AttributeFormatEnum.DATE_D_EEEE_H_MM){
            date_format = format.substring(7);
          }
          xlsx_width_cols.push(xlsx_width_col);
          row[label_i18n] = this.datePipe.transform(this.getDate(key, item), date_format);
          continue;
        }
        if(["string", "status", "address", "custom", "localized", "localizedTextarea"].indexOf(format) >= 0) {
          xlsx_width_cols.push(xlsx_width_col);
          let key_tr: string = this.get(key, item);
          if(!key_tr) {
            key_tr = empty;
          }
          row[label_i18n] = this.tr.instant(key_tr);
          continue;
        }
        switch (format) {
          case 'number':
            xlsx_width_cols.push(xlsx_width_col);
            let key_tr: string = this.getNumber(key, item)
            if(key_tr !== "0" && !key_tr) {
              key_tr = empty;
            }
            row[label_i18n] = this.tr.instant(key_tr);
            break;
          case 'boolean':
            xlsx_width_cols.push(xlsx_width_col);
            row[label_i18n] = this.tr.instant(this.getBoolean(key, item));
            break;
          case 'currency':
            xlsx_width_cols.push(xlsx_width_col);
            row[label_i18n] = this.tr.instant(this.getCurrency(key, item));
            break;
          case 'accordion':
            let accordion_items: any[] = this.getAccordion(key, item);

            if(!Array.isArray(accordion_items)) {
              accordion_items = [];
            }
            const accordion_length: number = accordion_items.length,
              accordion_arr: string[] = key.split('.'),
              accordion_field = (accordion_arr && accordion_arr[1]) ? accordion_arr[1] : '';
            let accordion_items_value: string = '';
            if(accordion_length === 0 || !accordion_field) {
              xlsx_width_cols.push(xlsx_width_col);
              row[label_i18n] = this.tr.instant(empty);
              break;
            }
            for(let k = 0; k < accordion_length; k++) {
              let accordion_item: any = accordion_items[k];
              if(!accordion_item || !accordion_item[accordion_field]) {
                continue;
              }
              const accordion_item_value: string = this.language.getLocalizedString(accordion_item[accordion_field]);
              accordion_items_value += (accordion_items_value) ? ', ' + accordion_item_value : accordion_item_value;
            }
            xlsx_width_cols.push(xlsx_width_col);
            row[label_i18n] = accordion_items_value ? accordion_items_value : empty;
            break;
          default:


        }
      }
      items.push(row);
      this.process_items_after_request = i;
    }
    const options: XlsxOptions = {cols: xlsx_width_cols};
    let table_name: string = this.tr.instant('dashboard.table');
    if(this.table_title) {
      table_name += '_' + this.tr.instant(this.table_title);
    }
    if(this.export_xlsx_exit) {
      return;
    }
    this.export_xlsx_exit = false;

    this.xlsxService.exportAsExcelFile(items, table_name, options);
    this.modalRefProgressExportXlsx.hide();
    this.process_items_after_request = 0;
    this.count_items_export = 0;
    this._items_export = [];
  } */

  protected executeFunctionByName(namespaces: string[], context: object): string | object {
    const func = namespaces.pop();
    for (let i = 0; i < namespaces.length; i++) {
      context = context[namespaces[i]];
    }
    const result = context[func];
    if (!result) {
      return 'dashboard.empty';
    }
    return result;
  }

  protected checkSubscription(subscription: Subscription) {
    if (!subscription || typeof subscription.unsubscribe !== 'function') {
      return;
    }
    subscription.unsubscribe();
  }

  protected attributesInit(): void {
    if (this.listener && typeof this.listener.onAttributesInit === 'function') {
      this.listener.onAttributesInit();
    }
  }

  getMax(): number {
    return this.max;
  }

  setConfig(): void {
    const config: PaginationModuleOptions = this.config;
    this.direction = config.direction;
    this.direction_default = config.direction;
    this.max = config.max;
    this._sort = config.sort;
    this._sort_default = config.sort;
  }

  private isValidComponent(component: any): boolean {
    return (
      component
      && typeof component['updateEventData'] === 'function'
      && typeof component['generateRequestUrl'] === 'function'
      && typeof component['updateEventDataByParamItem'] === 'function'
      && typeof component['getParamItem'] === 'function'
      && typeof component['getValueByKey'] === 'function'
    );
  }

}

