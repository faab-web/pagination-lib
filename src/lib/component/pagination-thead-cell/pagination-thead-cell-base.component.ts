import { PaginationEventData } from './../../interface/pagination-event-data';
import { PaginationProcessData } from './../../interface/pagination-process-data';
import { PaginationTheadCellSortingComponent } from './pagination-thead-cell-sorting.component';
import {
  PaginationTheadCellComponentInterface,
  PaginationTheadCellStaticInterface
} from './../../interface/pagination-thead-cell-component-interface';
import { PaginationEvent } from './../../model/pagination-event';
import { Input, EventEmitter, Output, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { PaginationSortingDirectionEnum } from '../../enum/pagination-sorting-direction.enum';
import { Subscription } from 'rxjs';
import { StaticImplements } from '../../decorator/static-implements.decorator';
import { PaginationSortingEventData } from '../../interface/pagination-sorting-event-data';
import { PaginationParamItem } from '../../interface/pagination-param-item';

@StaticImplements<PaginationTheadCellStaticInterface>()
export class PaginationTheadCellBaseComponent implements PaginationTheadCellComponentInterface, OnInit, OnDestroy, AfterViewInit {

  static EVENT_TYPE: string = '';
  static PARAM_NAME: string[] = [];

  @Input() key: string = '';
  @Input() title: string;
  @Input() direction: PaginationSortingDirectionEnum;
  @Input() sortable: boolean = false;
  @Output() changePaginationEvent: EventEmitter<PaginationEvent> = new EventEmitter<PaginationEvent>();

  constructor() {}

  public static updateEventData(event_data: PaginationEventData, process_data: PaginationProcessData): number {
    return 1;
  }

  public static updateEventDataByParamItem(param_item: PaginationParamItem): void {}

  public static getParamItem(): string[] {
    return [];
  }

  public static generateRequestUrl(): string {
    return '';
  }

  public static getValueByKey(key: string): any {}

  ngOnInit(): void {
    this.onInit();
  }

  onInit(): void {}

  ngOnDestroy(): void {
    this.onDestroy();
  }

  onDestroy(): void {}

  ngAfterViewInit(): void {
    this.afterViewInit();
  }

  afterViewInit(): void {}

  onChangeDirection(direction: PaginationSortingDirectionEnum) {
    const event: PaginationEvent = new PaginationEvent(
      PaginationTheadCellSortingComponent.EVENT_TYPE,
      {
        key: this.key,
        value: direction
      } as PaginationSortingEventData
    );
    this.changePaginationEvent.emit(event);
  }

  protected checkSubscription(subscription: Subscription) {
    if (!subscription || typeof subscription.unsubscribe !== 'function') {
      return;
    }
    subscription.unsubscribe();
  }
}
