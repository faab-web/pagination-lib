import { Subscription } from 'rxjs';
import { PaginationTheadCellPlainComponent } from './pagination-thead-cell-plain.component';
import { Component, OnInit, OnDestroy, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { PaginationTheadCellDirective } from '../../directive/pagination-thead-cell.directive';
import { ComponentFactoryResolver } from '@angular/core';
import { PaginationTheadCellComponentData } from '../../interface/pagination-thead-cell-component-data';
import { PaginationEvent } from '../../model/pagination-event';
import { PaginationTheadCellComponentInterface } from '../../interface/pagination-thead-cell-component-interface';
import { PaginationService } from '../../service/pagination.service';
import { PaginationModuleOptions } from '../../interface/pagination-module-options';

@Component({
  selector: 'lib-pagination-thead-cell',
  template: `<ng-template pagination-thead-cell-host></ng-template>`
})
export class PaginationTheadCellResolverComponent implements OnInit, OnDestroy {
  @Input() data: PaginationTheadCellComponentData;
  @Output() changePaginationEvent: EventEmitter<PaginationEvent> = new EventEmitter<PaginationEvent>();
  @ViewChild(PaginationTheadCellDirective) paginationTheadCellHost: PaginationTheadCellDirective;

  private filter_components: any[];
  private subscription_pagination_event: Subscription;
  private subscription_config: Subscription;

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private paginationService: PaginationService
  ) {
    this.subscription_config = this.paginationService.config.subscribe((config_new: PaginationModuleOptions) => {
      if (!config_new) {
        return;
      }
      this.setConfig(config_new);
    });
  }

  ngOnInit(): void {
    this.onInit();
  }

  onInit(): void {
    this.loadComponent();
  }

  ngOnDestroy(): void {
    this.onDestroy();
  }

  onDestroy(): void {
    this.checkSubscription(this.subscription_pagination_event);
    this.checkSubscription(this.subscription_config);
  }

  loadComponent(): void {
    const data: PaginationTheadCellComponentData = this.data;
    if (!data) {
      return;
    }
    const event_type: string = (typeof this.data.event_type === 'string') ? this.data.event_type
    : PaginationTheadCellPlainComponent.EVENT_TYPE;
    const component: any = this.paginationService.getComponentByEventType(event_type, PaginationTheadCellPlainComponent);
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(component);

    const viewContainerRef = this.paginationTheadCellHost.viewContainerRef;
    viewContainerRef.clear();

    const componentRef = viewContainerRef.createComponent(componentFactory);
    const component_instance: PaginationTheadCellComponentInterface = (<PaginationTheadCellComponentInterface>componentRef.instance);
    component_instance.key = data.key;
    component_instance.title = data.title;
    component_instance.sortable = data.sortable;
    component_instance.direction = data.direction;
    if (typeof data.value !== 'undefined') {
      component_instance.value = data.value;
    }
    this.checkSubscription(this.subscription_pagination_event);
    this.subscription_pagination_event = component_instance.changePaginationEvent.subscribe(
      (event: PaginationEvent) => {
        this.changePaginationEvent.emit(event);
      }
    );
  }

  protected checkSubscription(subscription: Subscription) {
    if (!subscription || typeof subscription.unsubscribe !== 'function') {
      return;
    }
    subscription.unsubscribe();
  }

  private setConfig(config: PaginationModuleOptions): void {
    this.filter_components = (config && Array.isArray(config.filter_components)) ? config.filter_components : [];
  }
}
