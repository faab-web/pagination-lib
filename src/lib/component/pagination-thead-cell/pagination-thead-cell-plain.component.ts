import { PaginationTheadCellBaseComponent } from './pagination-thead-cell-base.component';
import { Component } from '@angular/core';

@Component({
  selector: 'lib-pagination-base-thead-cell-plain',
  templateUrl: './pagination-thead-cell-plain.component.html',
  styleUrls: []
})
export class PaginationTheadCellPlainComponent extends PaginationTheadCellBaseComponent {
  static EVENT_TYPE = 'PLAIN';
}
