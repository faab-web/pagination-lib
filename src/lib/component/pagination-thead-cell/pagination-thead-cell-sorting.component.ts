import { PaginationSortingDirectionType } from '../../type/pagination-sorting-direction.type';
import { Input, EventEmitter, Component, OnDestroy, OnInit, Output } from '@angular/core';
import { PaginationSortingDirectionEnum } from '../../enum/pagination-sorting-direction.enum';
import { Subscription } from 'rxjs';
import { PaginationService } from '../../service/pagination.service';

@Component({
  selector: 'lib-pagination-base-thead-cell-sorting',
  templateUrl: 'pagination-thead-cell-sorting.component.html',
  styleUrls: ['pagination-thead-cell-sorting.component.scss'],
  providers: [PaginationService]
})
export class PaginationTheadCellSortingComponent implements OnInit, OnDestroy {

  static EVENT_TYPE: string = 'SORTING';

  static PARAM_NAME_DIRECTION: string = 'direction';
  static PARAM_NAME_SORT: string = 'sort';

  get direction(): PaginationSortingDirectionType {
    return this._direction;
  }

  @Input() set direction(value: PaginationSortingDirectionType) {
    this._direction = value;
  }

  @Input() title: string;
  @Input() key: string;
  @Output() clickChangeDirection: EventEmitter<PaginationSortingDirectionEnum> = new EventEmitter<PaginationSortingDirectionEnum>();

  private _direction: PaginationSortingDirectionType = null;
  private _subscription_direction_change: Subscription;

  constructor(private paginationService: PaginationService) {
    this._subscription_direction_change = this.paginationService.change.subscribe(
      (key: string) => {
        if (key !== this.key) {
          this._direction = null;
        }
      }
    );
  }

  ngOnInit(): void {
    this.onInit();
  }

  onInit(): void {}

  ngOnDestroy(): void {
    this.onDestroy();
  }

  onDestroy(): void {
    this.checkSubscription(this._subscription_direction_change);
  }

  onClick(e: MouseEvent) {
    if (!(e instanceof MouseEvent)) {
      return;
    }
    e.stopPropagation();
    e.preventDefault();
    this.paginationService.change.emit(this.key);
    if (this._direction === PaginationSortingDirectionEnum.ASC) {
      this._direction = PaginationSortingDirectionEnum.DESC;
    } else {
      this._direction = PaginationSortingDirectionEnum.ASC;
    }
    this.clickChangeDirection.emit(this._direction);
  }

  getStyleClass(): string {
    return (!this._direction) ? '' : this._direction;
  }

  getDirectionTitle(): PaginationSortingDirectionEnum {
    if (this.direction === null) {
      return PaginationSortingDirectionEnum.ASC;
    }
    return this.direction === PaginationSortingDirectionEnum.DESC ?
      PaginationSortingDirectionEnum.ASC : PaginationSortingDirectionEnum.DESC;
  }

  private checkSubscription(subscription: Subscription) {
    if (!subscription || typeof subscription.unsubscribe !== 'function') {
      return;
    }
    subscription.unsubscribe();
  }
}

