import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  // tslint:disable-next-line: directive-selector
  selector: '[pagination-thead-cell-host]',
})
export class PaginationTheadCellDirective {
  constructor(public viewContainerRef: ViewContainerRef) { }
}
