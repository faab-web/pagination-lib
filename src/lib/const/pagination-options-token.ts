import { PaginationModuleOptions } from '../interface/pagination-module-options';
import { InjectionToken } from '@angular/core';

export const PAGINATION_OPTIONS_TOKEN = new InjectionToken<PaginationModuleOptions>('PAGINATION_OPTIONS_TOKEN');
